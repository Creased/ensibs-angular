var controllers = angular.module('AppController', []);

// Contrôleur de la page d'accueil
controllers.controller('homeCtrl', ['$scope', '$http', function ($scope, $http) {
    $http.get('https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=parcours-des-lignes-de-bus-du-reseau-star')
    .then(function(response, status) {
        $scope.datas = response.data;
    });
    adjust();
}]);

// Contrôleur de la page about
controllers.controller('aboutCtrl', ['$scope', function ($scope) {
    $scope.text = 'Made with ❤️ by Baptiste MOINE';
    $scope.random = 'http://lorempicsum.com/futurama/640/320/' + Math.floor((Math.random()*9)+1);
    adjust();
}]);

// Contrôleur du menu de navigation
controllers.controller('headerCtrl', ['$scope', '$location', function ($scope, $location) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}]);
