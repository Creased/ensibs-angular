var app = angular.module('app', ['ngRoute', 'AppController']);

app.config(['$routeProvider', function ($routeProvider) {
    // Routage
    $routeProvider
    .when('/home', {
        templateUrl: 'partials/home.html',
        controller: 'homeCtrl'
    })
    .when('/about', {
        templateUrl: 'partials/about.html',
        controller: 'aboutCtrl'
    })
    .otherwise({
      redirectTo: '/home'
    });
}]);

function adjust() {
    $("body > main").css("min-height", $("body").outerHeight() - ($("footer").outerHeight() + $("header").outerHeight() + 40));
}
